#include "picturedisplaybackend.h"
#include <QStandardPaths>
#include <QFile>
#include <QJsonDocument>
#include <QNetworkRequest>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QMimeDatabase>
#include <QDir>
#include <QFileInfo>
#include "enc-util.h"

PictureDisplayBackend::PictureDisplayBackend(QObject *parent) : QObject(parent), manager( new QNetworkAccessManager(this) )
{
    connect(manager, &QNetworkAccessManager::finished, this, &PictureDisplayBackend::process);
}
PictureDisplayBackend::~PictureDisplayBackend() {
    delete manager;
}

QString PictureDisplayBackend::path() {
    return p;
}

void PictureDisplayBackend::load(QString mxc) {
    QUrl mxc_url(mxc);
    QString out_path(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/files/" + mxc_url.host());
    if (QDir(out_path).exists()) {
        for (QString str : QDir(out_path).entryList()) {
            qDebug() << "Found possible image" << str;

            if (str.startsWith(mxc_url.fileName() + ".")) {
                p = out_path + "/" + str;
                emit loaded();
                return;
            }
        }
    }
    QByteArray secret_stored = get_secret(&m_secretManager, config_secret_id());
    if (secret_stored != nullptr) {
        QJsonDocument document = QJsonDocument::fromJson(secret_stored);
        QString access_token = document.object().value("access_token").toString();
        QString hs_url = document.object().value("home_server").toString();


        QNetworkRequest req(hs_url + "/_matrix/media/r0/download/" + mxc_url.host() + "/" + mxc_url.fileName());
        req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));

        manager->get(req);
    }
}

void PictureDisplayBackend::process(QNetworkReply* reply) {
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << "Unable:" << reply->errorString() << reply->readAll();
        return;
    }

    QString hostname = reply->url().path().split("/").at(5);
    qDebug() << "GOT IMAGE:" << hostname;
    QString media_id = reply->url().fileName();
    QMimeDatabase db;
    QString ext = db.mimeTypeForName(reply->rawHeader("Content-Type")).preferredSuffix();
    QString out_path(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/images/" + hostname + "/" + media_id + "." + ext);
    QFile output(out_path);
    QDir output_dir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/images/" + hostname);
    output_dir.mkpath(output_dir.path());

    if (output.open(QFile::WriteOnly)) {
        qDebug() << "opened";
        output.write(reply->readAll());
        output.close();

        p = out_path;
        emit loaded();
    }
}

void PictureDisplayBackend::save(QString file_name) {
    QString picturesLocation = QStandardPaths::writableLocation(QStandardPaths::PicturesLocation);
    QString appPicturesLocation = picturesLocation + "/" + "Sailtrix/";
    QDir target_dir(picturesLocation);
    QDir app_dir(appPicturesLocation);
    if (target_dir.mkpath("Sailtrix")) {
        QString possible_file_name = file_name;
        QFileInfo fileinfo(file_name);
        int num = 0;
        while (app_dir.exists(possible_file_name)) {
            possible_file_name = fileinfo.baseName() + " (" + QString::number(++num) + ")." + fileinfo.completeSuffix();
        }

        QFile::copy(path(), appPicturesLocation + possible_file_name);
        emit image_saved();
    } else {
        emit image_error();
    }
}
