#ifndef MESSAGE_H
#define MESSAGE_H

#include <QObject>

class Message
{
public:

    Message(QString display_name, QString user_id, QString content, QString avatar_mxc, QString avatar);
    QString display_name();
    QString user_id();
    QString content();
    QString avatar_mxc();
    QString avatar();
    void set_display_name(QString &display_name);
    void set_user_id(QString &user_id);
    void set_content(QString &content);
    void set_avatar_mxc(QString &avatar_mxc);
    void set_avatar(QString &avatar);

private:
    QString m_display_name;
    QString m_user_id;
    QString m_content;
    QString m_avatar_mxc;
    QString m_avatar;

};

#endif // MESSAGE_H
