import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    property int numNotifs;

    Image {
           source: "/usr/share/icons/hicolor/172x172/apps/harbour-sailtrix.png"
           anchors {
               left: parent.left
               rightMargin: - (parent.width / 3)
               top: parent.top
               topMargin: - (parent.height / 4)

               right: parent.right
               leftMargin: (parent.width / 4)
               bottom: parent.bottom
               bottomMargin: (parent.height / 2)
           }
           fillMode: Image.PreserveAspectFit
           opacity: 0.2
       }

    Column {
        anchors.centerIn: parent
        anchors.top: parent.top
        anchors.topMargin: parent.height / 4


        Label {
            font.pixelSize: Theme.fontSizeLarge
            text: "Sailtrix"
        }

        Row {
            Label {
                font.pixelSize: Theme.fontSizeLarge
                text: numNotifs
            }

            Label {
                font.pixelSize: Theme.fontSizeSmall
                text: qsTr(" notifications")
                anchors.bottom: parent.bottom
            }
        }
    }
}
